let collection = [];

// Write the queue functions below.
//1
function print() {
    return collection;
}

//2
function enqueue(element) {
    collection[collection.length] = element;
    return collection;
}

function dequeue() {
    let [, ...newCollection] = collection;
    return collection = newCollection;
}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}


// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};